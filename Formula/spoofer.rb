class Spoofer < Formula
    desc "Command line tool to spoof your MAC address and scan your network"
    homepage "https://gitlab.com/julianfairfax/spoofer"
    url "https://gitlab.com/julianfairfax/spoofer/-/archive/3c5034417709e2622e438cb117b876778196f395/spoofer-3c5034417709e2622e438cb117b876778196f395.tar.gz"
    sha256 "27434bd804b3e317dda539e5b5b9d9521aecf523e1221c83c2f10e82c4fa4fe4"
    license "GPL"
    version "2.4.5"
  
    def install
        bin.install "spoofer.sh" => "spoofer"
        prefix.install Dir["resources/*"]
    end
end