#!/bin/bash

cd "${0%/*}"

name="filen-desktop-client"
version="1.6.2"
description="Filen Desktop Client for Windows, macOS and Linux"

mkdir "$name"

echo "pkgname=\"$name\"
pkgver=\"$version\"
arch=(\"any\")
pkgdesc=\""$description"\"
pkgrel=\"1\"
source=(\"https://github.com/FilenCloudDienste/filen-desktop/releases/download/1.6.2/filen-setup.AppImage\")
sha256sums=(\"5d0f29658c5c0e81db5de6ed3758b49f200198468e2e42f8883b6825e933a3e7\")

prepare() {
    chmod +x \"\${srcdir}\"/filen-setup.AppImage
    \"\${srcdir}\"/filen-setup.AppImage --appimage-extract
}

package() {
    install -d \"\${pkgdir}\"/opt/"$name"
    cp -r \"\${srcdir}\"/squashfs-root/. \"\${pkgdir}\"/opt/"$name"

    if [[ -f \"\${pkgdir}\"/opt/"$name"/default.desktop ]]; then
        mv \"\${pkgdir}\"/opt/"$name"/default.desktop \"\${pkgdir}\"/opt/"$name"/"$name".desktop
    fi

    sed -i \"s|AppRun|/opt/"$name"/"$name"|\" \"\${pkgdir}\"/opt/"$name"/"$name".desktop
    sed -i 's/Terminal\=true/Terminal\=false/' \"\${pkgdir}\"/opt/"$name"/"$name".desktop
    install -Dm644 \"\${pkgdir}\"/opt/"$name"/"$name".desktop -t \"\${pkgdir}\"/usr/share/applications

    install -Dm644 \"\${pkgdir}\"/opt/"$name"/usr/share/icons/hicolor/0x0/apps/"$name".png \"\${pkgdir}\"/usr/share/pixmaps/"$name".png
    
    install -d \"\${pkgdir}\"/usr/bin
    ln -s /opt/"$name"/"$name" \"\${pkgdir}\"/usr/bin
    chmod -R 755 \"\${pkgdir}\"/opt/"$name"
}" | tee "$name"/PKGBUILD