#!/bin/sh

mkdir -p pkgs/rpms

mkdir pkgs/debs 

git clone https://gitlab.com/julianfairfax/pi-capsule-project

chmod +x pi-capsule-project/build_deb.sh
pi-capsule-project/build_deb.sh

cp pi-capsule-project/*.deb /tmp

chmod +x pi-capsule-project/build_rpm.sh
pi-capsule-project/build_rpm.sh

git clone https://gitlab.com/julianfairfax/spoofer

chmod +x spoofer/build_deb.sh
spoofer/build_deb.sh

cp spoofer/*.deb /tmp

chmod +x spoofer/build_rpm.sh
spoofer/build_rpm.sh

chmod +x hydroxide/build_deb.sh
hydroxide/build_deb.sh

cp hydroxide/*.deb /tmp

git clone https://gitlab.com/julianfairfax/repackage

chmod +x repackage/build_deb.sh
repackage/build_deb.sh

cp repackage/*.deb /tmp

chmod +x repackage/build_rpm.sh
repackage/build_rpm.sh

cp /root/rpmbuild/RPMS/noarch/*.rpm pkgs/rpms

mkdir -p pkgs/debs/conf
touch pkgs/debs/conf/{option,distributions}
echo 'Codename: packages' >> pkgs/debs/conf/distributions
echo 'Components: main' >> pkgs/debs/conf/distributions
echo 'Architectures: amd64 i386 arm64 armhf' >> pkgs/debs/conf/distributions
echo 'SignWith: yes' >> pkgs/debs/conf/distributions



echo -e "\e[0;32mSign the repositories\e[0m"
openssl aes-256-cbc -d -in priv.gpg.enc -out priv.gpg -k "$PASSPHRASE"
gpg2 --import pub.gpg && gpg2 --import priv.gpg

expect -c "spawn gpg2 --edit-key C123CB2B21B9F68C80A03AE005B2039A85E7C70A trust quit; send \"5\ry\r\"; expect eof"
rpm --define "_gpg_name Julian Fairfax <juliannfairfax@protonmail.com>" --addsign pkgs/rpms/*rpm

createrepo_c --database --compatibility pkgs/rpms/
gpg2 --local-user "Julian Fairfax <juliannfairfax@protonmail.com>" --yes --detach-sign --armor pkgs/rpms/repodata/repomd.xml

reprepro -V -b pkgs/debs includedeb packages /tmp/*deb

echo -e "\e[0;32mList of imported public and private keys:"
gpg2 --list-keys && gpg2 --list-secret-keys

echo -e "\e[0;32mDeploy to Gitlab Pages...\e[0m"